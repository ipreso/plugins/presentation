// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * Description: Manage pictures for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Date:        $Date$
 *****************************************************************************/

#include "iPlugPresentation.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    iDebug ("[%s] initialization", PLUGINNAME);
    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin)) i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = NULL;
    (*plugins)[i].pClean     = dlsym (handle, "clean");
    i++;

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char filename [FILENAME_MAXLENGTH];
    char fullPath [FILENAME_MAXLENGTH];
    char colorSpace [32];
    char param [32];
    char value [32];
    char opt[32];
    int posFilename, i;
    FILE * fp = NULL;

    iDebug ("[%s] get command", PLUGINNAME);

    memset (colorSpace, '\0', sizeof (colorSpace));
    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));
    memset (opt, '\0', sizeof (opt));

    // Get the "aspect=X" parameter
    if (getParam (config, media, "aspect", value, sizeof (value)))
    {
        if (strncmp (value, "keep", sizeof (value)) == 0)
            strcpy (opt, "");
        else if (strncmp (value, "scale", sizeof (value)) == 0)
            strcpy (opt, "!");
        else
            strcpy (opt, "!");
    }
    else
        strcpy (opt, "!");
    
    memset (filename, '\0', FILENAME_MAXLENGTH);
    posFilename = strlen (PLUGINNAME) 
                    + strlen ("://")
                    + 32                // MD5 Size
                    + strlen ("/");
    for (i = 0 ; posFilename < strlen (media) &&
                    media [posFilename] != ':' ; i++, posFilename++)
    {
        filename [i] = media [posFilename];
    }

    // Check the file exists
    memset (fullPath, '\0', sizeof (fullPath));
    snprintf (fullPath, sizeof (fullPath), "%s/%s/%s/%s",
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);
    if (access (fullPath, R_OK))
    {
        iError ("[%s] File '%s' is not readable.", zone, fullPath);
        iDebug ("[%s] path_playlists: %s", PLUGINNAME, config->path_playlists);
        iDebug ("[%s] currentProgram.name: %s", PLUGINNAME, config->currentProgram.name);
        iDebug ("[%s] playlist_media: %s", PLUGINNAME, config->playlist_media);
        return (NULL);
    }

    // Get the colorspace
    //iDebug ("[%s] Get colorspace for '%s'...", PLUGINNAME, filename);
    memset (buffer, '\0', size);
    snprintf (buffer, size-1,
                "%s -format \"%%[colorspace]\" \"%s/%s/%s/%s\"",
                PLUGINPRES_COLORSPACE,
                config->path_playlists,
                config->currentProgram.name,
                config->playlist_media,
                filename);

    //iDebug ("[%s] Executing: '%s'", PLUGINNAME, buffer);
    fp = popen (buffer, "r");
    if (fp)
    {
        if (!fgets (colorSpace, sizeof (colorSpace) - 1, fp))
        {
            iDebug ("[%s] Cannot identify colorspace for '%s'", PLUGINNAME, filename);
            strncpy (colorSpace, "RGB", sizeof (colorSpace) - 1); 
        }
        //else
        //{
        //    iDebug ("[%s] Identified colorspace: '%s'", PLUGINNAME, colorSpace);
        //}
        pclose (fp);
    }
    else
    {
        iDebug ("[%s] Cannot get colorspace for '%s'", PLUGINNAME, filename);
        strncpy (colorSpace, "RGB", sizeof (colorSpace) - 1); 
    }

    // Create command line
    memset (buffer, '\0', size);
    snprintf (buffer, size-1, 
              "%s -quiet -colorspace %s -background black -resize %dx%d%s \"%s/%s/%s/%s\" &",
              PLUGINPRES_APP,
              colorSpace,
              width, height, opt,
              config->path_playlists,
              config->currentProgram.name,
              config->playlist_media,
              filename);

    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    char buffer [CMDLINE_MAXLENGTH];
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;

    iDebug ("[%s] preparation for WID %d", PLUGINNAME, wid);

    if (!shm_getApp (wid, &app))
        return (0);
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100, 
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
    system (buffer);
    return (1);
}

int play (sConfig * config, int wid)
{
    sApp app;
    int duration;
    char param [32];
    char value [32];
    int status;

    iDebug ("[%s] play for WID %d", PLUGINNAME, wid);

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 10;
    else
        duration = atoi (value);

    if (duration > 0)
    {
        iDebug ("[%s][%s] Sleep duration: %ds", PLUGINNAME, app.zone, duration);
        sleep (duration);
    }
    else
    {
        while ((status = sleep (1)) == 0) { }
    }

    return (1);
}

int clean ()
{
    iDebug ("[%s] Killing all instances of %s", PLUGINNAME, PLUGINPRES_APP);

    system ("pkill display");
    return (1);
}
