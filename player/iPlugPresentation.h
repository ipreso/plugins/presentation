// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * Description: Manage pictures for iAppCtrl
 * Author:      Marc Simonetti <marc.simonetti@geekcorp.fr
 * Date:        $Date$
 *****************************************************************************/

#include "iAppCtrl.h"

#define PLUGINNAME      "Presentation"
#define PLUGINPRES_APP "/usr/bin/display"
#define PLUGINPRES_COLORSPACE "/usr/bin/identify"



/*****************************************************************************/
int     initPlugin      (sPlugins * plugins, void * handle);
char *  getCmd          (sConfig * config,
                         char * zone, char * media,
                         int width, int height,
                         char * buffer, int size);
int     prepare         (sConfig * config, int wid);
int     play            (sConfig * config, int wid);
int     stop            (sConfig * config, int wid);
int     clean           ();
/*****************************************************************************/
